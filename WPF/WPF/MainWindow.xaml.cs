﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Excel;
using ExcelLib;
using WPF.Properties;
using Application = System.Windows.Application;
using TextBox = System.Windows.Controls.TextBox;
using Window = System.Windows.Window;

namespace WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Settings.Default.Save();
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private async void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            BlurEffectMainWindow.Radius = 10;
            GridLoading.Visibility=Visibility.Visible;
            var files = FolderIO.FolderIO.GetFilesInFolder(Settings.Default.Path);
            Excel.Instance.Value.CreateWorkbook("asd");
            Excel.Instance.Value.WriteToCell("Name", 1, 1);
            Excel.Instance.Value.WriteToCell("Date Created", 1, 2);
            var i = 2;
            foreach (var file in files)
            {
                Excel.Instance.Value.WriteToCell(file.Name, i, 1);
                Excel.Instance.Value.WriteToCell(file.CreationTime.ToString("g"), i, 2);
                i++;
            }
            
            GridLoading.Visibility=Visibility.Collapsed;
            BlurEffectMainWindow.Radius = 0;
        }

        private void ButtonBrowse_Click(object sender, RoutedEventArgs e)
        {
            BlurEffectMainWindow.Radius = 10;
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Settings.Default.Path = dialog.SelectedPath;
                Settings.Default.Save();
            }
            Console.WriteLine(result);
            BlurEffectMainWindow.Radius = 0;
        }
    }
}
