﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ExcelLib
{
    public class ExcelHelper
    {
        private Application _xlApplication = new Application {Visible = true};
        public void CreateWorkbook(string workBookName=null)
        {
            if (workBookName == null)
            {
                workBookName = "Dir2Excel";
            }
            
            _xlApplication.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
        }

        public void WriteToCell(string content, int x , int y )
        {
            var ws = (Worksheet) _xlApplication.Workbooks[1].Worksheets[1];
            bool failed = false;
            do
            {
                try
                {
                    ws.Cells[x, y] = content;
                    failed = false;
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    failed = true;
                }
                System.Threading.Thread.Sleep(10);
            } while (failed);
            
        }
    }
}
