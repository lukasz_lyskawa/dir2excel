﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelLib
{
    public static class Excel
    {
        public static Lazy<ExcelHelper> Instance { get; } = new Lazy<ExcelHelper>(()=> new ExcelHelper());
    }
}
