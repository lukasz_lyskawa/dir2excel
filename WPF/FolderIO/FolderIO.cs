﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderIO
{
    public static class FolderIO
    {


        public static List<FileInfo> GetFilesInFolder(string path,bool includeHidden=false)
        {
            var directory = new DirectoryInfo(@path);
            if (includeHidden)
            {
                return directory.GetFiles().ToList();
            }
            
            IEnumerable<FileInfo> files = directory.GetFiles();
            var filtered = from fileInfo in files
                    where (fileInfo.Attributes & FileAttributes.Hidden) == 0
                    select fileInfo;
            return filtered.ToList();
        }
    }
}
